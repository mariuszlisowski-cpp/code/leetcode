#include <algorithm>
#include <functional>
#include <initializer_list>
#include <iostream>
#include <iterator>
#include <stack>
#include <string>
#include <vector>

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
  public:
    bool isPalindrome(ListNode* head) {
        if (!head) {
            return true;
        }
        std::vector<int> list;
        while (head) {
            list.push_back(head->val);
            head = head->next;
        }
        std::vector<int> list_reversed;
        std::reverse_copy(list.begin(), list.end(),
                          std::back_inserter(list_reversed));

        return std::equal(list.begin(), list.end(), list_reversed.begin());
    } 

    // recursion
    bool isPalinRecur(ListNode* head) {
        std::function<bool(ListNode*)> ip_rec = [&](ListNode* p) -> bool {
            if(p) {
                if(!ip_rec(p->next) ) {
                    return false;
                }
                if( p->val != head->val ) {
                    return false;
                }
                head = head->next;
            }
            return true;
        };

        return ip_rec(head);
    }

    // stack method
    bool isPalinStack(ListNode* head){ 
        ListNode* curr = head;
        std::stack <int> stack;
        // copy list to stack
        while (curr) {
            stack.push(curr->val);
            curr = curr->next;
        }
        // iterate list and compare with stack
        while (head) {
            if (head->val != stack.top()) {
                return false;
            }
            stack.pop();
            head = head->next;
        }

        return true;
    } 
  
    ListNode* populate_list(std::initializer_list<int> list) {
        ListNode* head;
        ListNode* last;
        for (auto value : list) {
            ListNode* node = new ListNode(value);
            if (!head) {
                head = node;
                last = head;
            } else {
                last->next = node;
                last = last->next;
            }
        }

        return head;
    }

    void print_nodes(ListNode* head) {
        while (head != nullptr) {
            std::cout << head->val << ' ';
            head = head->next;
        }
        std::cout << std::endl;
    }
};

int main() {
    Solution solution;

    ListNode* head = solution.populate_list({-5, 4, 3, 4, -5});
    solution.print_nodes(head);

    std::cout << (solution.isPalindrome(head) ? "palindrome" : "NOT palindrome") << std::endl;
    std::cout << (solution.isPalinRecur(head) ? "palindrome" : "NOT palindrome") << std::endl;
    std::cout << (solution.isPalinStack(head) ? "palindrome" : "NOT palindrome") << std::endl;

    return 0;
}
