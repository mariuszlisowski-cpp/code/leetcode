#include <initializer_list>
#include <iostream>

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
  public:
    ListNode* reverseList(ListNode* head) {
        ListNode* curr = head;
        ListNode *prev{}, *next{};
        while (curr) {
            next = curr->next;
            curr->next = prev;
            prev = curr;
            curr = next;
        }
        
        return (head = prev);
    }

    ListNode* populate_list(std::initializer_list<int> list) {
        ListNode* head;
        ListNode* last;
        for (auto value : list) {
            ListNode* node = new ListNode(value);
            if (!head) {
                head = node;
                last = head;
            } else {
                last->next = node;
                last = last->next;
            }
        }

        return head;
    }

    void print_nodes(ListNode* head) {
        while (head != nullptr) {
            std::cout << head->val << ' ';
            head = head->next;
        }
        std::cout << std::endl;
    }
};

int main() {
    Solution solution;

    ListNode* head = solution.populate_list({1, 2, 3, 4, 5});
    solution.print_nodes(head);

    head = solution.reverseList(head);
    solution.print_nodes(head);

    return 0;
}
