#include <initializer_list>
#include <iostream>
#include <string>

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

// |1|-> |2|-> |3|-> |4|-> |5|-> null
class Solution {
  public:
    ListNode* removeNthFromEnd(ListNode* head, int n) {
        if (!head) {
            return head;
        }
        if (n <= 0) {
            std::cout << "> out of range!" << std::endl;
            return head;
        }
        ListNode* fast = head;
        ListNode* slow = head;

        // set position of fast at n+1
        for (size_t i = 1; i <= n + 1; ++i) {
            if (fast) {
                fast = fast->next;
            } else {
                // head to delete
                if (i == n + 1) {
                    ListNode* to_delete = head;
                    head = head->next;
                    delete to_delete;
                    return head;
                // do not exceed the list length
                } else {
                    std::cout << "> out of range!" << std::endl;
                    return head;
                }
            }
        }
        std::cout << "> slow: " <<  (slow ? std::to_string(slow->val) : "nullptr") << std::endl;
        std::cout << "> fast: " << (fast ? std::to_string(fast->val) : "nullptr") << std::endl;

        while (fast) {
            slow = slow->next;
            fast = fast->next;
        }
        std::cout << "> slow: " <<  (slow ? std::to_string(slow->val) : "nullptr") << std::endl;
        std::cout << "> fast: " << (fast ? std::to_string(fast->val) : "nullptr") << std::endl;
        
        ListNode* to_delete = slow->next;
        slow->next = slow->next->next;
        delete to_delete;

        return head;
    }

    ListNode* populate_list(std::initializer_list<int> list) {
        ListNode* head;
        ListNode* last;
        for (auto value : list) {
            ListNode* node = new ListNode(value);
            if (!head) {
                head = node;
                last = head;
            } else {
                last->next = node;
                last = last->next;
            }
        }

        return head;
    }

    void print_nodes(ListNode* head) {
        while (head) {
            std::cout << head->val << ' ';
            head = head->next;
        }
        std::cout << std::endl;
    }
};

int main() {
    Solution solution;

    ListNode* head {solution.populate_list({1, 2, 3, 4, 5})};
    solution.print_nodes(head);

    head = solution.removeNthFromEnd(head, 1);
    solution.print_nodes(head);

    return 0;
}
