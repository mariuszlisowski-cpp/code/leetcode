#include <algorithm>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <unordered_set>
#include <vector>

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(nullptr) {}
};

class Solution {
public:
    void deleteNode(ListNode* node) {
        // return if null or tail node
        if (node == nullptr || node->next == nullptr) {
            return;
        }
        ListNode* temp = node->next;
        node->val = node->next->val;
        node->next = node->next->next;
        delete temp;        
    }

    ListNode* populateList(int list[], size_t size) {
        if (size == 0) {
            return nullptr;
        }
        ListNode* head = new ListNode(list[0]);
        ListNode* temp = head;
        for (size_t i = 1; i < size; ++i) {
            temp->next = new ListNode(list[i]);
            temp = temp->next;
        }

        return head;
    }

    void printNodes(ListNode* head) {
        while(head != nullptr) {
            std::cout << head->val << ' ';
            head = head->next;
        }
        std::cout << std::endl;
    }
};

int main() {
    Solution solution;
    int list[] {4, 5, 1, 9};
    size_t size = sizeof(list) / sizeof(*list);

    ListNode* head = solution.populateList(list, size);
    solution.printNodes(head);

    solution.deleteNode(head->next->next);  // 1
    solution.printNodes(head);

    return 0;
}
