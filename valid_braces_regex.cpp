#include <iostream>
#include <regex>
#include <stack>
#include <map>

bool validBracesRegex(std::string str) {
  std::regex reg("\\(\\)|\\[\\]|\\{\\}");
  while (std::regex_search(str, reg)) {
      str = std::regex_replace(str, reg, "");
  }

  return str.length() == 0;
}

bool validBracesMapStackA(const std::string& braces,
                  std::stack<char> s = {},
                  std::map<char, char> dict = {{'(',')'}, {'[',']'}, {'{','}'}})
{
    std::for_each(braces.begin(), braces.end(),
                  [&](int x) {
                      (!s.empty() && dict[s.top()]==x) ?
                          (s.pop()) :
                          (s.push(x));
                  });
    return s.empty();
}

bool validBracesMapStackB(std::string str){
    std::map<char, char> braces{
        {')', '('}, 
        {'}', '{'}, 
        {']', '['}
    };
    std::stack<char> stackedBraces;
    for(char ch : str) {
        if(braces.find(ch) == braces.end()) {
            stackedBraces.push(ch);
        } else {
            if(!stackedBraces.empty() && braces[ch] == stackedBraces.top()) {
                stackedBraces.pop();
            } else {
                return false;
            }
        }
    }
    
    return stackedBraces.empty();
}

bool validBracesSwitch(const std::string& str) {
    std::stack<char> s;
    for (char c : str) {
        switch (c) {                                    // accepts braces only
            case '(': s.push(')'); break;
            case '[': s.push(']'); break;
            case '{': s.push('}'); break;
            case ')':
            case ']':
            case '}':
                if (!s.empty() && s.top() == c) {
                    s.pop();
                } else  {
                    return false;
                }
        }
    }

    return s.empty();
}

int main() {
    std::string invalid = "([){}])";
    std::string valid = "({ hello })[]{}";

    std::cout << (validBracesRegex(invalid) ? "Valid" : "Invalid") << ",\t";
    std::cout << (validBracesRegex(valid) ? "Valid" : "Invalid") << std::endl;

    std::cout << (validBracesMapStackA(invalid) ? "Valid" : "Invalid") << ",\t";
    std::cout << (validBracesMapStackA(valid) ? "Valid" : "Invalid") << std::endl;

    std::cout << (validBracesSwitch(invalid) ? "Valid" : "Invalid") << ",\t";
    std::cout << (validBracesSwitch(valid) ? "Valid" : "Invalid") << std::endl;

}
