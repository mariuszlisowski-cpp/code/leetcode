/* find three numbers whose product is maximum  */

#include <algorithm>
#include <functional>
#include <iostream>
#include <vector>

void print_vector(const std::vector<int> nums) {
	for (auto el : nums) {
		std::cout << el << ' ';
	}
	std::cout << std::endl;
}

int maximumProduct(std::vector<int>& nums) {
	constexpr size_t max_product_of = 3;
    std::sort(nums.begin(), nums.end());

	print_vector(nums);
	
	int product{};
	size_t size = nums.size();
	if (size >= max_product_of) {
		// is last number negative
		if (nums[size - 1] < 0) {
			
			std::cout << "> all negative:  ";
			product = nums[size - 1] * nums[size - 2] * nums[size - 3];			// all negative (last three negative)

		} else if (nums[0] * nums[1] > nums[size - 2] * nums[size - 3]) {
			
			std::cout << "> mixed numbers: ";
			product = nums[0] * nums[1] * nums[size - 1];						// mixed (two first neg. & last pos.)
			
		} else {

			std::cout << "> all positive:  ";
			product = nums[size - 1] * nums[size - 2] * nums[size - 3];			// all positive (last three positive)

		}
	}

	return product;
}

int main() {
	// note: -a * -a * a = 3*a but  -a * -a * -a = -3*a
	std::vector<int> vecA{-100, -3, -2, -1}; 	// -6 (-1 * -2 * -3)
	std::vector<int> vecB{-8, -7, -2, 10, 20}; 	// 1120 (-8 * -7 * 20)
	std::vector<int> vecC{1, 2, 3, 100}; 		// 600 (100 * 3 * 2)

	std::cout << maximumProduct(vecA) << std::endl;
	std::cout << maximumProduct(vecB) << std::endl;
	std::cout << maximumProduct(vecC) << std::endl;

	return 0;
}
