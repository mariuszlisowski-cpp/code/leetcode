#include <iostream>
#include <iterator>
#include <string>

class Solution {
    std::string check_around_center(std::string::iterator left, std::string::iterator right,
                                    const std::string& s, long& max_length, std::string& longest)
    {
        while (left >= s.begin() && right != s.end() && *left == *right) {
            auto current_length = std::distance(left, right);
            if (current_length > max_length) {
                longest = std::string(left, right + 1); // last iterator excluded thus incremented
                max_length = current_length;
            }
            --left;     // expand left
            ++right;    // expand right
        }

        return longest;
    }

public:
    std::string longest_palindrome(std::string s) {
        if (s.size() == 1) {
            return s;
        }
        long max_length{-1};
        std::string longest{};
        
        // expand around center (around every letter) [time complexity O(n2)]
        for (auto it{s.begin()}; it != s.end(); ++it) {
            // odd length
            check_around_center(it, it, s, max_length, longest);
            // even length
            check_around_center(it, it + 1, s, max_length, longest);
        }
        
        return longest;
    }
};

int main() {
	std::string strA{"babad"}; // 'bab' or 'aba'
	std::string strB{"coc_iolloi ala olla"}; // 'ioolloi'
	// std::string str{"abbc"}; // 'bb'
	// std::string str{"c"}; // 'c'

    Solution solution;
    std::cout << solution.longest_palindrome(strA) << std::endl;
    std::cout << solution.longest_palindrome(strB) << std::endl;

	return 0;
}
