#include <algorithm>
#include <iostream>
#include <map>
#include <vector>

template <typename T>
void printArray(T array) {
    for (int value : array) {
        std::cout << value << ' ';
    }
    std::cout << std::endl;
}

class Solution {
public:
    // O(n^2)
    std::vector<int> twoSum(std::vector<int>& nums, int target) {
        std::vector<int> result;
        for (auto it1 = nums.begin(); it1 != nums.end(); ++it1) {
            for (auto it2 = it1 + 1; it2 != nums.end(); ++it2) {
                if (*it1 + *it2 == target) {
                    result.emplace_back(it1 - nums.begin());
                    result.emplace_back(it2 - nums.begin());
                }
            }
        }

        return result;
    }

    // O(n)
    std::vector<int> twoSumOnePass(std::vector<int>& nums, int target) {
        std::map<int, int> map;
        for (int i = 0; i < nums.size(); i++) {
            int complement = target - nums.at(i);
            auto it = map.find(complement); // finds element with specific key 
            if (it != map.end()) {
                return std::vector<int>{it->second, i}; // found: index in map, current index
            }
            map.emplace(nums.at(i), i); // put value from vector, current index
        }

        return std::vector<int>{}; // nothing found
    }

};

int main() {
    Solution solution;
    std::vector<int> array {3, 2, 4}; // [1, 2] because nums[1] + nums[2] == 6 

    std::vector<int> result = solution.twoSumOnePass(array, 6);

    printArray(result);

    return 0;
}
