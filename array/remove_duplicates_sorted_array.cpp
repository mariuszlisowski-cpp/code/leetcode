#include <iostream>
#include <algorithm>
#include <vector>

class Solution {
public:
    int removeDuplicates(std::vector<int>& nums) {
        std::sort(nums.begin(), nums.end());
        nums.erase(std::unique(nums.begin(), nums.end()),             
                   nums.end());

        return nums.size();
    }
};

int main() {
    Solution solution;
    std::vector<int> array {0, 0, 1, 1, 1, 2, 2, 3, 3, 4};

    std::cout << solution.removeDuplicates(array) << std::endl;
    
    for (int value : array) {
        std::cout << value << ' ';
    }
    std::cout << std::endl;
    
    return 0;
}
