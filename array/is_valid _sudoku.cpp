#include <algorithm>
#include <iostream>
#include <map>
#include <vector>
#include <unordered_set>

template <typename T>
void printArray(T array) {
    for (int value : array) {
        std::cout << value << ' ';
    }
    std::cout << std::endl;
}

class Solution {
public:
    bool isValidSudoku(std::vector<std::vector<char>>& board) {
        const size_t ZERO_ASCII = 48;
        const size_t SUDOKU = 3;
        const size_t SUDOKU_HEIGHT = board.size() / SUDOKU;
        const size_t SUDOKU_WIDTH = board.at(0).size() / SUDOKU;

        size_t sudoku_height = SUDOKU_HEIGHT;
        size_t sudoku_width = SUDOKU_WIDTH;
        size_t column_index{};
        size_t row_index{};
        std::vector<int> nums;

        // (3x3)x9
        while (sudoku_height--) {
            while (sudoku_width--) {
                for (size_t i = 0; i < SUDOKU; i++) {
                    for (size_t j = 0; j < SUDOKU; j++) {
                        int value = static_cast<int>(board[i + row_index][j + column_index]) - 
                                    ZERO_ASCII;
                        if (value > 0) {
                            nums.emplace_back(value);
                        }
                    }
                    if (containsDuplicates(nums)) {
                        return false;
                    }
                }
                // printArray(nums);
                column_index += SUDOKU;
                nums.clear();
            }
            row_index += SUDOKU;
            column_index = 0;
            sudoku_width = SUDOKU_WIDTH;
        }

        // 9x9
        // horizontal
        for (auto it = board.begin(); it != board.end(); ++it) {
            for (auto itt = (*it).begin(); itt != (*it).end(); ++itt) {
                int value = static_cast<int>(*itt) - ZERO_ASCII;
                if (value > 0) {
                    nums.emplace_back(value);
                }
            }
            // printArray(nums);
            if (containsDuplicates(nums)) {
                return false;
            }
            nums.clear();
        } 
        // vertical
        for (size_t i = 0; i < SUDOKU_WIDTH * SUDOKU; i++) {
            for (size_t j = 0; j < SUDOKU_HEIGHT * SUDOKU; j++) {
                int value = static_cast<int>(board[j][i]) - ZERO_ASCII;
                if (value > 0) {
                    nums.emplace_back(value);
                }
            }
            // printArray(nums);
            if (containsDuplicates(nums)) {
                return false;
            }
            nums.clear();
        }
        
        return true;
    }

private:
    template <typename T>
    bool containsDuplicates(T& array) {
        std::unordered_set uniques(begin(array), end(array));

        return uniques.size() != array.size();
    }
};

int main() {
    Solution solution;
    std::vector<std::vector<char>> board = { 
        {'5','3','.',  '.','7','.',  '.','.','.'},
        {'6','.','.',  '1','9','5',  '.','.','.'},
        {'.','9','8',  '.','.','.',  '.','6','.'},

        {'8','.','.',  '.','6','.',  '.','.','3'},
        {'4','.','.',  '8','.','3',  '.','.','1'},
        {'7','.','.',  '.','2','.',  '.','.','6'},
        
        {'.','6','.',  '.','.','.',  '2','8','.'},
        {'.','.','.',  '4','1','9',  '.','.','5'},
        {'.','.','.',  '.','8','.',  '.','7','9'}
    }; // has to be a rectangle

    std::cout << std::boolalpha << solution.isValidSudoku(board);

    return 0;
}
