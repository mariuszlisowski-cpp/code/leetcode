// Given a non-empty array of decimal digits representing a non-negative integer, increment one to the integer

#include <algorithm>
#include <iostream>
#include <vector>

template <typename T>
void printArray(T array) {
    for (int value : array) {
        std::cout << value << ' ';
    }
    std::cout << std::endl;
}

class Solution {
public:
    std::vector<int> plusOne(std::vector<int>& digits) {
        for (auto it = digits.rbegin(); it != digits.rend(); ++it) {
            if (*it == 9) {
                if (std::distance(it, digits.rend()) == 1) {
                    *it = 0;
                    digits.emplace(digits.begin(), 1);
                    break;
                } else {
                    *it = 0;
                }
            } else {
                (*it)++;
                break;
            }
        }

        return digits;
    }
};

int main() {
    Solution solution;
    // std::vector<int> array {6,1,4,5,3,9,0,1,9,5,1,8,6,7,0,5,5,4,4}; // ...5545
    // std::vector<int> array {0, 8, 9, 9, 9}; // 09000
    std::vector<int> array {9}; // 10

    std::vector<int> result = solution.plusOne(array);

    printArray(result);

    return 0;
}
