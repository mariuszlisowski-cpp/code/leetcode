/*
first bad version
leetcode.com

Scenario #1: isBadVersion(mid) => false

 1 2 3 4 5 6 7 8 9
 G G G G G G B B B       G = Good, B = Bad
 |       |       |
left    mid    right


Scenario #2: isBadVersion(mid) => true

 1 2 3 4 5 6 7 8 9
 G G G B B B B B B       G = Good, B = Bad
 |       |       |
left    mid    right
*/

#include <iostream>

class Solution {
public:
    bool isBadVersion(int ver) {
        if (ver >= 5)
            return true;
        else
            return false;
    }
    int firstBadVersion(int n) {
        int min {1}, max{n}, half;
        while (min < max) {
            half = min + (max - min) / 2;
            std::cout << "Half: " << half << std::endl;
            // true
            if (isBadVersion(half)) {
                 max = half;
                 std::cout << "Max: " << max << std::endl;
            }
            // false
            else {
                min = half + 1;
                std::cout << "Min: " << min << std::endl;
            }
        }
        std::cout << "First bad version: ";
        return min;
    }
};

int main() {
    Solution test;

    std::cout << test.firstBadVersion(10) << std::endl;

    return 0;
}
