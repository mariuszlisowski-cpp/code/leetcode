#include <algorithm>
#include <iostream>
#include <map>
#include <vector>
#include <unordered_set>

template <typename T>
void printArray(T array) {
    for (int value : array) {
        std::cout << static_cast<char>(value);
    }
    std::cout << std::endl;
}

class Solution {
public:
    // divide at half & swap left with right 
    void reverseString(std::vector<char>& s) {
        for (size_t i = 0; i < s.size() / 2; i++) {
            char temp = s[i];
            s[i] = s[s.size() - i - 1];    
            s[s.size() - i - 1] = temp;
        }
    }    
    
    //  bitwise swap
    void reverseStringNoTemp(std::vector<char>& s) {
        for (size_t i = 0; i < s.size() / 2; i++) {
            // x = x ^ y ^ (y = x);
            s[i] = s[i] ^ s[s.size() - i - 1] ^ (s[s.size() - i - 1] = s[i]);
        }
    }    
};

int main() {
    Solution solution;
    std::vector<char> cString {'r','a','i','s','i', 'n'};

    // solution.reverseString(cString);
    solution.reverseStringNoTemp(cString);

    printArray(cString);

    return 0;
}
