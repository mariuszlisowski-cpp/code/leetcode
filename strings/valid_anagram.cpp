#include <algorithm>
#include <iostream>
#include <map>
#include <vector>
#include <unordered_set>

class Solution {
public:
    bool isAnagram(std::string s, std::string t) {
        return std::is_permutation(s.begin(), s.end(), t.begin(), t.end());
    }

    bool isAnagramAlgo(std::string s, std::string t) {
        if (s.length() != t.length()) {
            return false;
        }
        std::sort(s.begin(), s.end());
        std::sort(t.begin(), t.end());

        return s == t;
    }
};

int main() {
    Solution solution;

    std::string s {"anagram"}, 
                t {"nagaram"};

    std::cout << std::boolalpha << solution.isAnagramAlgo(s, t);

    return 0;
}
