#include <algorithm>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <unordered_set>
#include <vector>

class Solution {
public:
    std::string countAndSay(int n) {
        if (n == 1) {
            return "1";
        }
        std::string base = "1";
        for (int i = 1; i < n; i++) {
            std::stringstream ss;
            int count = 1;
            char ch = base[0];
            for (int j = 1; j < base.length(); j++) {
                if (base[j] != ch) {
                    ss << count << ch;
                    count = 1;
                    ch = base[j];
                } else {
                    count++;
                }
            }
            ss << count << ch;
            base = ss.str();
        }

        return base;
    }
};

int main() {
    Solution solution;

    std::cout << solution.countAndSay(4);  // "1211"

    return 0;
}
//  in    out
//  1     1
//  2     11
//  3     21
//  4     1211
//  5     111221
//  6     312211
//  7     13112221
