#include <algorithm>
#include <iostream>
#include <map>
#include <vector>
#include <unordered_set>

class Solution {
public:
    bool isPalindrome(std::string s) {
        if (!s.empty()) {
            std::string alphanum = removeNonAlphanumericChars(s);
            for (size_t i = 0; i < alphanum.size(); i++) {
                if (alphanum[i] != alphanum[alphanum.size() - i - 1]) {
                    return false;
                }
            }
        }

        return true;
    }

private:
    std::string removeNonAlphanumericChars(std::string& str) {
        std::string result{};
        for (auto &&ch : str) {
            if (std::isalnum(ch)) {
                result += std::tolower(ch);
            }
        }

        return result;
    }
};

int main() {
    Solution solution;

    std::string s {"A man, a plan, a canal: Panama"}; 
    // std::string s {"racecar"}; 

    std::cout << s << std::endl << std::boolalpha << solution.isPalindrome(s);

    return 0;
}
