#include <algorithm>
#include <iostream>
#include <map>
#include <vector>
#include <unordered_set>

template <typename T>
void printArray(T array) {
    for (int value : array) {
        std::cout << static_cast<char>(value);
    }
    std::cout << std::endl;
}

class Solution {
public:
     int reverse(int x) {
        int result{};
        while (x != 0) {
            int reminder = (x % 10);
            // detect overflow
            if (result > INT_MAX / 10 || (result == INT_MAX / 10 && reminder > 7)) return 0;
            if (result < INT_MIN / 10 || (result == INT_MIN / 10 && reminder < -8)) return 0;

            result = result * 10 + reminder;
            x /= 10;
        }
        return result;
    }    
};

int main() {
    Solution solution;
    
    std::cout << solution.reverse(-123450); // OK
    
    // std::cout << solution.reverse(1534236469); // overflow
    // 964632435 * 10 = 9_646_324_350 (INT_MAX 2_147_483_647)

    return 0;
}
