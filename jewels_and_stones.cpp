// jewels and stones
// leetcode.com
// ~ how many letters from string A in string B (combined)

#include <iostream>
#include <unordered_map>

class Solution {
public:
    int numJewelsInStones(std::string J, std::string S) {
        int count {};
        std::unordered_map<char, int> mp;
        for (auto ch : S)
            mp[ch]++;
        for (auto ch : J)
            count += mp[ch];
        return count;
    }
};

int main() {
    std::string A {"Ab"};
    std::string B {"aAAbbbb"};

    Solution obj;

    std::cout << obj.numJewelsInStones(A, B);

    return 0;
}
